# dev-webapi-dotnet

This is a Asp.Net Core WebAPI and is written in C#. It has the function of taking a request with longitude and latitude and then querying OpenWeatherMap API with these values. It then takes the response JSON and desrializes it into an object. With this object we can serialize it into its own response object with only the relavant properties needed such as "temperature", "condition" etc.

```bash
curl --location --request GET 'http://devwebapidotnet-dev.eu-west-1.elasticbeanstalk.com/api/v1/weather?longitude=53.3498&latitude=-6.2603'
```
The API endpoint can be hit with a GET request to http://devwebapidotnet-dev.eu-west-1.elasticbeanstalk.com/api/v1/weather and takes the parameters "longitude" and "latitude"

The expected response is JSON object.
```bash
{
    "temperature": 28.62,
    "icon": "02d",
    "condition": "Clouds",
    "windSpeed": 4.59,
    "humidity": 71
}
```

##### WebAPI Testing#####
The WebAPI has Tests to confirm when longitude and latitude are requested it returns a 200 response. 

```bash
    public class WeatherControllerTests
    {
        private readonly WeatherController _controller;
        public IConfiguration _config;


        public WeatherControllerTests()
        {
            _config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _controller = new WeatherController(_config);
        }

        [Fact]
        public async Task TestGet()
        {
            var result = await _controller.Get("53.3498", "6.2603") as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
    }
```
##### WebAPI Error Handling#####
The API checks if the request contains a value for latitude and longitude and returns a BadRequest if it is missing either. There are also Try Catch blocks when making a request to OpenWeatherMap API and returns any error caught with that request.

```bash
		[HttpGet]
        public async Task<IActionResult> Get(string latitude, string longitude, string units = "metric")
        {
            //Check request isn't empty

            if (string.IsNullOrEmpty(latitude) && string.IsNullOrEmpty(longitude))
            {
                return BadRequest("Full Longitude and Latitude Missing");
            }
```