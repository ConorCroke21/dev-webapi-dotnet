﻿using dev_webapi_dotnet.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace dev_webapi_dotnet.Tests
{
    public class WeatherControllerTests
    {
        private readonly WeatherController _controller;
        public IConfiguration _config;


        public WeatherControllerTests()
        {
            _config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _controller = new WeatherController(_config);
        }

        [Fact]
        public async Task TestGet()
        {
            var result = await _controller.Get("53.3498", "6.2603") as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
    }

}

