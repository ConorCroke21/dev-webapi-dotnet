﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dev_webapi_dotnet.Models
{
    public class WeatherConditions
    {
        public double Temperature { get; set; }
        public string Icon { get; internal set; }
        public string Condition { get; set; }
        public double WindSpeed { get; set; }
        public long Humidity { get; set; }

        public void AssignWeatherAttributes(OpenWeatherJsonResponse openWeatherJson)
        {
            Condition = openWeatherJson.Weather[0].Main;
            Icon = openWeatherJson.Weather[0].Icon;
            Humidity = openWeatherJson.Main.Humidity;
            Temperature = openWeatherJson.Main.Temp;
            WindSpeed = openWeatherJson.Wind.Speed;
        }
    }
}
