﻿using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp.Serialization.Json;
using dev_webapi_dotnet.Models;
using Microsoft.Extensions.Configuration;
using RestSharp.Deserializers;

namespace dev_webapi_dotnet.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IConfiguration _config;


        public WeatherController(IConfiguration config)
        {
            _config = config;
        }

        // GET: api/v1/<WeatherController>
        [HttpGet]
        public async Task<IActionResult> Get(string latitude, string longitude, string units = "metric")
        {
            //Check request isn't empty

            if (string.IsNullOrEmpty(latitude) && string.IsNullOrEmpty(longitude))
            {
                return BadRequest("Full Longitude and Latitude Missing");
            }

            try
            {
                //Assign OpenWeather details

                string openWeatherUrl = _config.GetValue<string>("OpenWeatherSettings:URL");
                string openWeatherApiKey = _config.GetValue<string>("OpenWeatherSettings:APIKey");

                //Make Request

                string url = $"{openWeatherUrl}?lat={latitude}&lon={longitude}&appid={openWeatherApiKey}&units={units}";
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    //Deserialize Openweather response and assign to Api response

                    var openWeatherJson = JsonConvert.DeserializeObject<OpenWeatherJsonResponse>(response.Content);

                    WeatherConditions weatherConditions = new WeatherConditions();
                    weatherConditions.AssignWeatherAttributes(openWeatherJson);
                    
                    return Ok(weatherConditions);
                }
                else
                {
                    return BadRequest(response.Content);
                }
            }
            catch (Exception)
            {

                return StatusCode(500);
            }
        }
    }
}
